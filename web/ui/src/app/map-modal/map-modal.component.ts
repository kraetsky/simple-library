import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss']
})
export class MapModalComponent implements OnInit {

  latitude = 51.733038;
  longitude = 39.182408;
  mapType = 'satellite';
  zoom = 16;

  constructor(private dialogRef: MatDialogRef<MapModalComponent>) {
  }

  ngOnInit() {
  }


  close() {
    this.dialogRef.close();
  }

}
