import {Component, OnInit} from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormBuilder,
  FormControl,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {UsersService} from '../service/users.service';
import {Router} from '@angular/router';
import {Observable} from "rxjs";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(8)]],
    login: ['', [Validators.required, Validators.minLength(6)], [this.forbiddenLoginValidator(this.usersService)]],
    password: ['', [Validators.minLength(8)]],
    email: ['', [Validators.pattern("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])")],
      [this.forbiddenEmailValidator(this.usersService)]],
    phoneNumber: ['', [Validators.pattern("^(\\+7|7|8)?[\\s\\-]?\\(?[489][0-9]{2}\\)?[\\s\\-]?[0-9]{3}[\\s\\-]?[0-9]{2}[\\s\\-]?[0-9]{2}$")]],
    isActive: true
  });

  constructor(private fb: FormBuilder,
              private usersService: UsersService,
              private router: Router) {
  }

  ngOnInit() {

  }

  register() {
    if (this.registerForm.valid) {
      this.usersService.create(this.registerForm.getRawValue())
        .subscribe(() => {
          this.router.navigate(['login']);
          this.router.navigate(['login'], {queryParams: {register: 'true'}});
        });
    }
  }

  getOnlyLetters(event) {
    const reg = /[^а-яА-ЯёЁa-zA-Z\-\s]/g;
    if (event.key.search(reg) !== -1) {
      this.registerForm.patchValue({name: this.registerForm.get('name').value.replace(reg, '')});
    }
  }

  removeCyrillic(event) {
    const reg = /[а-яА-ЯёЁ]/g;
    if (event.key.search(reg) !== -1) {
      this.registerForm.patchValue({email: this.registerForm.get('email').value.replace(reg, '')});
    }

  }


  forbiddenLoginValidator(usersService: UsersService): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      return usersService.checkLoginExists(control.value);
    };
  }

  forbiddenEmailValidator(usersService: UsersService): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
      return usersService.checkEmailExists(control.value);
    };
  }

}
