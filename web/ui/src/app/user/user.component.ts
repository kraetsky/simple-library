import {Component, OnInit} from '@angular/core';
import {UsersService} from '../service/users.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FileService} from '../service/file.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  selectedFile: File;
  imageUrl = 'https://www.civhc.org/wp-content/uploads/2018/10/question-mark.png';
  baseUrl;

  userForm = this.fb.group({
    name: '',
    login: '',
    password: '',
    email: '',
    phoneNumber: '',
    picUid: '',
    isActive: true
  });

  constructor(private usersService: UsersService,
              private fb: FormBuilder,
              private fileService: FileService,
              private router: Router) {
  }

  ngOnInit() {
    this.baseUrl = window.location.origin;
    this.usersService.getCurrentUser().subscribe(res => {
      this.userForm = this.fb.group(res);
      if (res.picUid) {
        this.imageUrl = this.baseUrl + '/api/file/get/' + res.picUid;
      }
    });

  }

  onFileChanged(event) {
    if (event.target.files.length === 0)
      return;
    this.selectedFile = event.target.files[0];

    var reader = new FileReader();
    reader.readAsDataURL(this.selectedFile);
    reader.onload = (_event) => {
      this.imageUrl = reader.result;
    }
  }


  save() {
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile);
    uploadData.set('type', 'IMAGE_PNG');
    this.fileService.uploadFile(uploadData).subscribe(res => {
      this.imageUrl = this.baseUrl + '/api/file/get/' + res;
      this.userForm.patchValue({picUid: res});
      this.usersService.create(this.userForm.getRawValue())
        .subscribe(() => this.router.navigate(['']));
    });

  }

}
