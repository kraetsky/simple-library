import {Component} from '@angular/core';
import {AuthService} from './service/auth.service';
import {Router} from '@angular/router';
import {MatDialog} from "@angular/material/dialog";
import {MapModalComponent} from "./map-modal/map-modal.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Simple Library';

  constructor(private authService: AuthService,
              private router: Router,
              private matDialog: MatDialog) {

  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }

  isLoggedOut() {
    return this.authService.isLoggedOut();
  }

  openMap() {
    this.matDialog.open(MapModalComponent, {
      width: '410px',
      height: '410px',
    });
  }
}
