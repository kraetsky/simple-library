import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";
import {Author} from "../domain/author";
import {ModalMode} from '../domain/modal-mode';
import {AuthorService} from "../service/author.service";

@Component({
  selector: 'app-authors',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss']
})
export class AuthorListComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns = ['id', 'name', 'biography'];
  datasource: MatTableDataSource<Author> = new MatTableDataSource<Author>();
  authors: Author[];
  ModalMode = ModalMode;

  constructor(private authorService: AuthorService) {
  }

  ngOnInit() {
    this.loadAuthors();
  }

  loadAuthors() {
    this.authorService.getAll().subscribe(data => {
      this.authors = data;
      this.datasource = new MatTableDataSource<Author>(this.authors);
      this.datasource.paginator = this.paginator;
      this.datasource.sort = this.sort;
    });
  }
}
