import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable()
export class FileService {

  constructor(private http: HttpClient) {
  }

  public uploadFile(data: FormData): Observable<string> {
    return this.http.post('api/file/upload', data).pipe(map(res => res['data']));
  }

  public getFile(uid: string): Observable<any> {
    return this.http.get(`api/file/${uid}`).pipe(map(res => res['data']));
  }

}
