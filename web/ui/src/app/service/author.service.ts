import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {Author} from "../domain/author";


@Injectable()
export class AuthorService {

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<Author[]> {
    return this.http.get('api/authors/all').pipe(map(res => res['data'] as Author[]));
  }

}
