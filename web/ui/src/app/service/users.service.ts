import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../domain/user';
import {Observable} from 'rxjs/Observable';
import {map} from 'rxjs/operators';

@Injectable()
export class UsersService {

  constructor(private http: HttpClient) {
  }

  public getCurrentUser(): Observable<User> {
    return this.http.get('api/users/current').pipe(map(res => res['data'] as User));
  }

  public create(user): Observable<User> {
    return this.http.post('api/users', user).pipe(map(res => res['data'] as User));
  }

  public getById(id: number): Observable<User> {
    return this.http.get(`api/users/${id}`).pipe(map(res => res['data'] as User));
  }

  public checkLoginExists(login: string) {
    return this.http.get(`api/users/check_login_exists`, {params: {login: login}})
      .pipe(map(res => {
        return !res['data'] ? null : {'forbiddenLogin': true}
      }));
  }

  public checkEmailExists(email: string) {
    return this.http.get(`api/users/check_email_exists`, {params: {email: email}})
      .pipe(map(res => {
        return !res['data'] ? null : {'forbiddenEmail': true}
      }));
  }

}
