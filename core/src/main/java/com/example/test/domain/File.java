package com.example.test.domain;

import com.example.test.enums.FileTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name = "files")
public class File {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Basic()
    private byte[] content;
    @Column(name = "file_type")
    @Enumerated(value = EnumType.STRING)
    private FileTypeEnum fileType;
    private String uid;
}
