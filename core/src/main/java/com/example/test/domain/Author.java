package com.example.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "authors")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String biography;
    @Column(name = "pic_uid")
    private String picUid;
    @OneToMany(mappedBy = "author")
    @JsonIgnore
    private List<Book> books;
}


