package com.example.test.config;

import org.hibernate.dialect.PostgreSQL10Dialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;


import javax.sql.DataSource;
import java.util.Properties;

@Configuration
public class HibernateConfig {

    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean factory = new LocalSessionFactoryBean();
        try {
            factory.setDataSource(dataSource);
            factory.setPackagesToScan("com.example.test.domain");
            Properties props = new Properties();
            props.put("hibernate.dialect", PostgreSQL10Dialect.class);
            factory.setHibernateProperties(props);
        } catch (Exception ex) {
            ex.getStackTrace();
        }
        return factory;
    }

    @Bean
    public HibernateTransactionManager transactionManager(LocalSessionFactoryBean sessionFactory) {
        HibernateTransactionManager tx = new HibernateTransactionManager();
        tx.setSessionFactory(sessionFactory.getObject());
        return tx;
    }
}
