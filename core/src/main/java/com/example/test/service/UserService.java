package com.example.test.service;

import com.example.test.common.exception.ServiceException;
import com.example.test.dao.UserDao;
import com.example.test.domain.User;
import com.example.test.service.communication.MailService;
import com.example.test.service.communication.SendgridMailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class UserService {

    private final UserDao userDao;
    private final PasswordEncoder encoder;
    private final MailService mailService;
    private final ResourceLoader resourceLoader;
    private final SendgridMailService sendgridMailService;
    @Value("${app.smtp.from}")
    private String fromMail;
    @Value("${app.base-url}")
    private String baseUrl;

    public User getById(Long id) {
        log.trace("Getting a user with id {}.", id);
        User user = userDao.getById(id).orElseThrow(() -> new ServiceException("There is no user with id {}.", id));
        log.trace("Getting a user with id {} has completed successfully.", id);
        return user;
    }

    public User getCurrentUser() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDao.getByLogin(user.getLogin()).orElseThrow(() -> new ServiceException("Current user cannot be identified."));
    }

    public User getByLogin(String login) {
        log.trace("Getting a user with login {}.", login);
        User user = userDao.getByLogin(login).orElseThrow(() -> new ServiceException("There is no user with login {}.", login));
        log.trace("Getting a userUser with login {} has completed successfully.", login);
        return user;
    }

    public User create(User user) {
        log.trace("Creating a user with login {}.", user.getLogin());
        user.setPassword(encoder.encode(user.getPassword()));
        if (user.getIsActive() == null) {
            user.setIsActive(true);
        }
        user = userDao.createOrUpdate(user);
        log.trace("Creating a user with login {} has completed successfully.", user.getLogin());
        HashMap<String, String> params = new HashMap<>();
        params.put("name", user.getName());
        params.put("baseUrl", baseUrl);
        String message = getMessage("registration_template.html", params);
        sendgridMailService.send(message, fromMail, user.getEmail(), "Registration in Simple Library");
        return user;
    }

    public User update(User user) {
        log.trace("Updating a user with login {}.", user.getLogin());
        if (encoder.matches(user.getPassword(), getById(user.getId()).getPassword())) {
            user.setPassword(encoder.encode(user.getPassword()));
        }
        user.setPassword(encoder.encode(user.getPassword()));
        user = userDao.createOrUpdate(user);
        log.trace("Updating a user with login {} has completed successfully.", user.getLogin());
        return user;
    }

    public Boolean checkLoginExists(String login) {
        log.trace("Checking if a user with login {} exists.", login);
        Optional<User> user = userDao.getByLogin(login);
        return user.isPresent();
    }

    public Boolean checkEmailExists(String login) {
        log.trace("Checking if a user with login {} exists.", login);
        Optional<User> user = userDao.getByEmail(login);
        return user.isPresent();
    }

    private String getMessage(String fileName, HashMap<String, String> params) {
        String message = null;
        try {
            Resource templateFile = resourceLoader.getResource("classpath:messages/" + fileName);
            java.util.Scanner s = new java.util.Scanner(templateFile.getInputStream()).useDelimiter("\\A");
            message = s.hasNext() ? s.next() : "";
        } catch (IOException e) {
            log.error("Failed to read message.");
        }
        return replacementMessage(message, params);
    }

    private static String replacementMessage(String message, HashMap<String, String> params) {
        for (Map.Entry<String, String> item : params.entrySet()) {
            message = message.replace("{" + item.getKey() + "}", item.getValue());
        }
        return message;
    }

}

