package com.example.test.service.communication;

import com.example.test.domain.File;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;

import org.springframework.stereotype.Service;


import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class MailService {

//    private final JavaMailSender sender;

    public void send(String text, String from, String to, String subject) {
        send(text, from, to, subject, null);
    }

    public void send(String text, String from, String to, String subject, List<File> attachments) {
//        try {
//            log.debug("Sending email to: '{}' with subject: '{}'.", to, subject);
//            MimeMessagePreparator messagePreparator = mimeMessage -> {
//                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
//                messageHelper.setFrom(from);
//                messageHelper.setTo(to);
//                messageHelper.setSubject(subject);
//                messageHelper.setText(text, true);
//                if (attachments != null && attachments.size() > 0) {
//                    attachments.forEach(attachment -> {
//                        try {
//                            messageHelper.addAttachment(attachment.getName(), new ByteArrayResource(attachment.getContent()));
//                        } catch (MessagingException e) {
//                            log.error("Unexpected error occurred while attaching files to E-Mail.", e);
//                        }
//                    });
//                }
//            };
//            sender.send(messagePreparator);
//            log.debug("Mail has been sent successfully.");
//        } catch (Exception e) {
//            log.error("Unexpected error occurred while sending E-Mail.", e);
//        }
    }
}
