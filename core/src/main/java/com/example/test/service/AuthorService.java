package com.example.test.service;

import com.example.test.dao.AuthorDao;
import com.example.test.domain.Author;
import com.example.test.domain.Book;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class AuthorService {

    private final AuthorDao authorDao;

    public List<Author> getAll() {
        log.trace("Getting all authors.");
        List<Author> authors = authorDao.getAll();
        log.trace("Getting all authors has finished successfully.");
        return authors;
    }
}
