package com.example.test.service.communication;

import com.sendgrid.*;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
@RequiredArgsConstructor
public class SendgridMailService {

    private final SendGrid sendGrid;

    public void send(String text, String from, String to, String subject) {
        log.debug("Sending email to: '{}' with subject: '{}'.", to, subject);
        Email fromEmail = new Email(from);
        Email toEmail = new Email(to);
        Content content = new Content("text/html", text);
        Mail mail = new Mail(fromEmail, subject, toEmail, content);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendGrid.api(request);
            System.out.println(response.getStatusCode());
            log.debug("Sending email to: '{}' with subject: '{}' has been set successfully.", to, subject);
        } catch (IOException ex) {
            log.error("Unexpected error occurred while sending E-Mail: {}.", ex.getMessage());
        }
    }
}
