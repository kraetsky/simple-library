package com.example.test.endpoint;

import com.example.test.domain.api.Response;
import com.example.test.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/authors")
public class AuthorEndpoint {

    private final AuthorService authorService;

    @GetMapping("/all")
    public Response getAll() {
        return Response.from(() -> authorService.getAll());
    }

}
