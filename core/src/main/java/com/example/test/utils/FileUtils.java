package com.example.test.utils;

import com.example.test.domain.File;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public class FileUtils {

    public static HttpEntity<byte[]> toHttpEntity(File file, boolean download) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE,
                "image/png");
        headers.setContentLength(file.getContent().length);
        if (download) {
            headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.getName());
        }
        return new HttpEntity<>(file.getContent(), headers);
    }
}
