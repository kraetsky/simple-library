package com.example.test.dao;

import com.example.test.domain.Book;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class BookDao {

    private final SessionFactory sessionFactory;

    public List<Book> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("from books")
                .getResultList();
    }

    public Optional<Book> getById(Long id) {
        return sessionFactory.getCurrentSession()
                .createQuery("from books where id = :bookId")
                .setParameter("bookId", id)
                .uniqueResultOptional();
    }

    public Book saveOrUpdate(Book book) {
        sessionFactory.getCurrentSession()
                .saveOrUpdate(book);
        return book;
    }


//    public List<OrderEntity> all(OrderFilter filter) {
//        EntityManager entityManager = sessionFactory.createEntityManager();
//        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
//        CriteriaQuery<OrderEntity> query = builder.createQuery(OrderEntity.class);
//        Root<OrderEntity> root = query.from(OrderEntity.class);
//
//        Join<OrderEntity, ThemeExcursionEntity> joinTheme = root.join("themeExcursion", JoinType.LEFT);
//
//        List<Predicate> predicates = toPredicates(filter, root, joinTheme, joinPaymentStatus);
//        query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));
//        if (filter.getSortDirection() != null && filter.getSortField() != null) {
//            query.orderBy(toSortOrder(filter, root, joinTheme, joinPaymentStatus, joinGuide, joinLanguage));
//        }
//        TypedQuery<OrderEntity> typedQuery = entityManager.createQuery(query);
//        if (filter.getPageIndex() != null && filter.getPageSize() != null) {
//            int startPos = ((filter.getPageIndex()) * filter.getPageSize());
//            typedQuery.setFirstResult(startPos);
//            typedQuery.setMaxResults(filter.getPageSize());
//        }
//
//        List<OrderEntity> result = typedQuery.getResultList();
//        entityManager.close();
//        return result;
//    }

    public void delete(Long id) {
        sessionFactory.getCurrentSession()
                .createQuery("delete from books where id = :bookId")
                .setParameter("bookId", id)
                .executeUpdate();
    }

}
