package com.example.test.dao;

import com.example.test.domain.Author;
import com.example.test.domain.Book;
import lombok.RequiredArgsConstructor;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class AuthorDao {

    private final SessionFactory sessionFactory;

    public List<Author> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("from authors")
                .getResultList();
    }

}
