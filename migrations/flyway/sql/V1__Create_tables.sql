CREATE TABLE files
(
id         SERIAL PRIMARY KEY,
content    BYTEA,
file_type  TEXT,
name       TEXT,
uid        TEXT NOT NULL
);

CREATE TABLE users
(
    id           SERIAL PRIMARY KEY,
    login        TEXT   NOT NULL,
    password     TEXT   NOT NULL,
    name         TEXT   NOT NULL,
    email        TEXT,
    phone_number TEXT,
    is_active    BOOLEAN NOT NULL DEFAULT TRUE,
    pic_uid      TEXT
);

CREATE TABLE books
(
id SERIAL PRIMARY KEY,
name TEXT NOT NULL,
author TEXT,
release_year TEXT,
user_id INTEGER REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
number_or_pages INTEGER,
is_available BOOLEAN DEFAULT FALSE
);

