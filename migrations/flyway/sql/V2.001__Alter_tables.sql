CREATE TABLE authors
(
    id        SERIAL PRIMARY KEY,
    name      TEXT NOT NULL,
    biography TEXT,
    pic_uid   TEXT
);


ALTER TABLE books
    RENAME COLUMN number_or_pages TO number_of_pages;

ALTER TABLE books
    DROP COLUMN author,
    ADD COLUMN author_id BIGINT REFERENCES authors (id) ON UPDATE CASCADE ON DELETE RESTRICT;
