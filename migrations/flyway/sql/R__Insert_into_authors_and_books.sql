INSERT INTO users(id, login, password, name, email, phone_number, is_active)
VALUES (1, 'admin', '$2y$10$pLNJB0gN0JttQOdmogV0FOvmaEqAVQTiexcKGAWL3FvlsBmkh227m', 'Kirill', 'test@mail.ru',
        '+78213912320', TRUE)
ON CONFLICT(id) DO NOTHING;

INSERT INTO authors (id, name, biography, pic_uid)
VALUES (1, 'William Shakespeare',
        '(1564 – 1616) English poet and playwright. Famous plays include Macbeth, Romeo and Juliet, Merchant of Venice and Hamlet. Shakespeare is widely considered the seminal writer of the English language.',
        null),
       (2, 'Johann Wolfgang von Goethe',
        '(1749 – 1832) German poet, playwright, and author. Notable works of Goethe include Faust, Wilhelm Meister’s Apprenticeship and Elective Affinities.',
        null),
       (3, 'Samuel Johnson',
        '(1709 – 1784) British author best-known for his compilation of the English dictionary. Although not the first attempt at a dictionary, it was widely considered to be the most comprehensive – setting the standard for later dictionaries.',
        null),
       (4, 'Jonathan Swift',
        '(1667 – 1745) Anglo-Irish writer born in Dublin. Swift was a prominent satirist, essayist and author. Notable works include Gulliver’s Travels (1726), A Modest Proposal and A Tale of a Tub.',
        null),
       (5, 'Jane Austen',
        '(1775 – 1817) English author who wrote romantic fiction combined with social realism. Her novels include Sense and Sensibility (1811), Pride and Prejudice (1813) and Emma (1816).',
        null),
       (6, 'Honore de Balzac',
        '(1799 – 1850) French novelist and short story writer. Balzac was an influential realist writer who created characters of moral ambiguity – often based on his own real-life examples. His greatest work was the collection of short stories La Comédie Humaine.',
        null),
       (7, 'Alexandre Dumas',
        '(1802 – 1870) French author of historical dramas, including – The Count of Monte Cristo (1844), and The Three Musketeers (1844). Also prolific author of magazine articles, pamphlets and travel books.',
        null),
       (8, 'Victor Hugo',
        '(1802 – 1885) French author and poet. Hugo’s novels include Les Misérables, (1862) and Notre-Dame de Paris (1831).',
        null),
       (9, 'Charles Dickens',
        '(1812 – 1870) – English writer and social critic. His best-known works include novels such as Oliver Twist, David Copperfield and A Christmas Carol.',
        null),
       (10, 'Charlotte Bronte',
        '(1816 – 1855) English novelist and poet, from Haworth. Her best-known novel is ‘Jane Eyre’ (1847).',
        null),
       (11, 'Unknown author',
        '',
        null),
       (12, 'J.R.R. Tolkien',
        '(1892 – 1973) was an English writer, poet, philologist, and academic. He was the author of the classic high fantasy works The Hobbit and The Lord of the Rings.',
        null),
       (13, 'Joshua Bloch',
        '',
        null)

ON CONFLICT(id) DO NOTHING;

INSERT INTO books(id, name, user_id, release_year, number_of_pages, is_available, author_id)
VALUES (1, 'The Hobbit', 1, 2003, 1050, TRUE, 12),
       (2, 'Pride and Prejudice', 1, 1956, 157, TRUE, 5),
       (3, 'The Great Gatsby', 1, 1925, 300, TRUE, null),
       (4, 'A Tale Of Two Cities', 1, 1945, 200, TRUE, null),
       (5, 'Effective Java', 1, 2005, 1200, TRUE, 13),
       (6, 'Mastering Java', 1, 2001, 920, TRUE, 13),
       (7, 'The Catcher in the Rye', 1, 1995, 320, TRUE, null),
       (8, 'Become Rich in 3 Month', 1, 2005, 2320, TRUE, null)

ON CONFLICT(id) DO NOTHING;
